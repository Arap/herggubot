# HerGGuBot #


```
#!Shell

Version: 0.1.7 (11.5.2016)
```


Module based Node.js TeamSpeak 3 bot. It utilizes TeamSpeaks own ServerQuery API and is fully configurable. 

###Install###
Check that you have npm installed.
Run this oneliner to install:
```
#!shell

git clone https://bitbucket.org/Arap/herggubot.git; cd herggubot; npm install 

```
To run the bot: 
```
#!shell

node startHerggubot.js

```

###Installing with Docker###
[What is Docker?](https://docs.docker.com/)

Working DockerFile is provided with the repository:
```
#!shell

git clone https://bitbucket.org/Arap/herggubot.git; cd herggubot;

[arnold@___ herggubot]$ docker build  -t herggu/herggubot --no-cache . 
...
...
...
Successfully built 5c95d6700e6f
[arnold@____ herggubot]$ docker run -p 127.0.0.1:9090:9090 -d 5c95d6700e6f
200a157b082efef4a7a8a307a9a9df245682ea819bb110e8222095074fed939b

```

### Configuration ###


### License ###

```
#!license

The MIT License (MIT)

Copyright (c) 2015 HerGGuBot, HerGGu-team Arttu Siren <arttu.siren@gmail.com> , Hugo Kiiski <hugo.kiiski@hotmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```


### Library documentation and help for developing ###

[node-sqlite3](https://github.com/mapbox/node-sqlite3/wiki/API)

[node-teamspeak](https://www.npmjs.com/package/node-teamspeak)

[Serverquery pdf](http://media.teamspeak.com/ts3_literature/TeamSpeak%203%20Server%20Query%20Manual.pdf)